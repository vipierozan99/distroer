#! /bin/python

from pathlib import Path
from typing import List
from distroer import *
import shutil
import click
from configs import *
from utils.debug import done_msg, loading_msg, error_msg
from colorama import Fore


@click.group()
def cli():
    pass


def apply_modules(modules: List[str], mount_dir: Path) -> None:
    for mod in modules:
        done_msg(f"Found module : {mod} applying...")
        MOD_PATH = MODULES_DIR/mod
        stdout = exec_chroot_script(MOD_PATH, mount_dir)
        done_msg(f"Module {mod} applied, the output was:")
        print(stdout.decode())


@click.command(help="Creates a new distro using a base image")
@click.argument("name", type=str)
@click.argument("base_image", type=click.Path(exists=True))
@click.option(
    "--module",
    "-m",
    multiple=True,
    help="Module names to be used")
@click.option("--root-partition", "-r", default=2)
def create(name, base_image, module, root_partition):
    modules = list(module)
    WORK_DIR = BUILD_DIR/name
    MNT_DIR = WORK_DIR/"mount"
    try:
        print(
            f"[{Fore.GREEN}STARTED{Fore.RESET}] Building new {name} distro", flush=True)
        WORK_DIR.mkdir(parents=True, exist_ok=True)
        MNT_DIR.mkdir(parents=True, exist_ok=True)
        OUTPUT_IMG = WORK_DIR/f"{name}.img"
        loading_msg("Copying base image to workspace")
        shutil.copy2(base_image, OUTPUT_IMG)
        done_msg("Copying base image to workspace")

        loading_msg(f"Mounting image to {MNT_DIR}")
        unmount_img(MNT_DIR)
        mount_image(OUTPUT_IMG, root_partition, MNT_DIR)
        done_msg(f"Mounting image to {MNT_DIR}")

        # fix network
        resolv = MNT_DIR/"etc/resolv.conf"
        resolv_old = MNT_DIR/"etc/resolv.conf.old"
        exec_cmd(f"mv {resolv} {resolv_old}")
        exec_cmd(f"cp /etc/resolv.conf {resolv}")

        apply_modules(modules, MNT_DIR)

        # cleanup
        exec_cmd(f"mv {resolv_old} {resolv}")
        unmount_img(MNT_DIR)

        done_msg(f"{name} distro build succeded! Output at: {OUTPUT_IMG}")

    except Exception as e:
        error_msg(f"{name} build failed! Unmounting...")
        raise e
    finally:
        unmount_img(MNT_DIR)


@click.command(help="Pathces an existing distro")
@click.argument("image", type=click.Path(exists=True))
@click.option(
    "--module",
    "-m",
    multiple=True,
    help="Module names to be used")
@click.option("--root-partition", "-r", default=2)
def patch(image, module, root_partition):
    modules = list(module)
    MNT_DIR = Path("/tmp/custom_pi_os/mount")
    OUTPUT_IMG = Path(image)
    try:
        MNT_DIR.mkdir(parents=True, exist_ok=True)

        loading_msg(f"Mounting image to {MNT_DIR}")
        unmount_img(MNT_DIR)
        mount_image(OUTPUT_IMG, root_partition, MNT_DIR)
        done_msg(f"Mounting image to {MNT_DIR}")

        # fix network
        resolv = MNT_DIR/"etc/resolv.conf"
        resolv_old = MNT_DIR/"etc/resolv.conf.old"
        exec_cmd(f"mv {resolv} {resolv_old}")
        exec_cmd(f"cp /etc/resolv.conf {resolv}")

        apply_modules(modules, MNT_DIR)

        # cleanup
        exec_cmd(f"mv {resolv_old} {resolv}")
        unmount_img(MNT_DIR)
        done_msg(
            f"{OUTPUT_IMG.name} distro patch succeded! Output at: {OUTPUT_IMG}")

    except Exception as e:
        error_msg(f"{OUTPUT_IMG.name} patch failed! Unmounting...")
        raise e
    finally:
        unmount_img(MNT_DIR)


@click.command(help="Mount an iso")
@click.argument("image", type=click.Path(exists=True))
@click.argument("mount_path", type=click.Path(), default=Path("/tmp/custom_pi_os/mount"))
@click.option("--root-partition", "-r", default=2)
def mount(image, mount_path, root_partition):
    try:
        MNT_DIR = Path(mount_path)
        MNT_DIR.mkdir(parents=True, exist_ok=True)
        OUTPUT_IMG = Path(image)
        unmount_img(MNT_DIR)
        mount_image(OUTPUT_IMG, root_partition, MNT_DIR)
        print(MNT_DIR)
    except:
        unmount_img(Path(mount_path))
        print("Mounting failed")


@click.command(help="Unmount an iso")
@click.argument("mount_path", type=click.Path(), default=Path("/tmp/custom_pi_os/mount"))
def unmount(mount_path):
    try:
        MNT_DIR = Path(mount_path)
        unmount_img(MNT_DIR)
        print(MNT_DIR)
    except:
        print("Unmounting failed")


cli.add_command(create)
cli.add_command(patch)
cli.add_command(mount)
cli.add_command(unmount)

if __name__ == '__main__':
    cli()
