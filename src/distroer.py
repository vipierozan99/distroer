from pathlib import Path
import os
import stat
import shutil
import pwd
from pprint import pprint
from configs import BUILD_DIR
from utils.sfdisk_if import SfdiskInfo, get_image_info, apply_image_layout
from utils.mount_if import mount_loopback, unmount_img
from utils.dd_if import append_zeroes
from utils.lsblk_if import get_blkdevices_info
from utils.subprocess_util import exec_cmd


def unpack(src: Path, dest: Path, owner=None) -> None:
    '''copy all files & folders from source to target, preserving mode and timestamps and chown to user.
    If user is not provided, no chown will be performed'''
    # copy owner and group
    st = os.stat(src)
    # copy content, stat-info (mode too), timestamps...
    shutil.copy2(src, dest)
    if owner:
        owner_id = pwd.getpwnam(owner).pw_uid
        os.chown(dest, owner_id, owner_id)
    else:
        os.chown(dest, st[stat.ST_UID], st[stat.ST_GID])


# TODO
def detach_loopback_devices(img_path: Path) -> None:
    ''' Cleans up mounted loopback devices from the image name '''
    # NOTE: it might need a better way to grep for the image name, its might clash with other builds
    pass


def expand_ext_part(img_path: Path, partition: int, size: int) -> SfdiskInfo:
    '''will enlarge partition number <partition> on /path/to/image by <size> MB'''
    img_info = get_image_info(img_path)
    append_zeroes(img_path, size)
    img_info.partitions[-1].size = None  # will default to max size
    img_info = apply_image_layout(img_path, img_info)
    loop_device = list(filter(lambda x: Path(x.mountpoint) ==
                              img_path, get_blkdevices_info()))[0]

    exec_cmd(f"e2fsck -fy {loop_device.name}")
    exec_cmd(f"resize2fs -p {loop_device.name}")

    return img_info


# TODO
def minimize_ext_part() -> None:
    pass


def mount_image(img_path: Path, root_partition: int, mount_path: Path) -> None:
    img_info = get_image_info(img_path)
    detach_loopback_devices(img_path)
    mount_path.mkdir(parents=True, exist_ok=True)
    mount_loopback(img_path, mount_path,
                   img_info.partitions[root_partition-1].start * 512)

    pass


def exec_chroot_script(module_path: Path, mount_path: Path) -> bytes:
    qemu_path = Path(shutil.which("qemu-arm-static"))
    shutil.copy2(qemu_path, mount_path/"usr/bin"/qemu_path.name)
    loaded_mod_path = mount_path/"tmp"/module_path.name
    shutil.rmtree(loaded_mod_path, ignore_errors=True)
    shutil.copytree(module_path, loaded_mod_path)

    cmd = exec_cmd(
        f"chroot {mount_path} usr/bin/qemu-arm-static /bin/bash /tmp/{module_path.name}/main")

    # cleanup
    shutil.rmtree(loaded_mod_path)
    return cmd.stdout
