from pathlib import Path

PROJECT_ROOT: Path = Path(__file__).absolute().parent.parent
BASE_IMAGES: Path = PROJECT_ROOT/"base_images"
BUILD_DIR: Path = PROJECT_ROOT/"build"
MODULES_DIR: Path = PROJECT_ROOT/"modules"
