

from __future__ import annotations
import os
from pprint import pprint
import subprocess as sb
import shlex
from pathlib import Path
import signal
from typing import List
from dataclasses import dataclass
from utils.subprocess_util import exec_cmd


@dataclass
class MountedInfo:
    fs_spec: str
    fs_file: str
    fs_vfstype: str
    fs_mntopts: str


def get_mounted_info() -> List[MountedInfo]:
    '''Get all mounted filesystems info'''
    cmd = exec_cmd(f"mount")

    result = []
    for line in cmd.stdout.decode().splitlines():
        [spec, rest] = line.split(" on ")
        [file, rest] = rest.split(" type ")
        [vfstype, mntopts] = rest.split(" ")
        result.append(MountedInfo(
            fs_spec=spec.strip(),
            fs_file=file.strip(),
            fs_vfstype=vfstype.strip(),
            fs_mntopts=mntopts.strip()
        ))

    return result


def kill_procs_using_path(path: Path) -> None:
    '''Kills all processes with a handle in path'''
    process = sb.run(shlex.split(
        f"sudo lsof {path}"), capture_output=True)
    if process.returncode != 0 and process.returncode != 1:
        raise sb.SubprocessError(process.stderr.decode())

    # awk '{print $2}'
    lines = process.stdout.decode().splitlines()[1:]
    pids = (int(f[1]) for f in (s.split() for s in lines))
    for pid in pids:
        os.kill(pid, signal.SIGKILL)


def mount_loopback(src=Path, dest=Path, offset=int) -> None:
    exec_cmd(f"sudo mount -o loop,offset={offset} {src} {dest}/")


def unmount_path(path: Path) -> None:
    exec_cmd(f"sudo umount {path}")


def unmount_img(mount_path: Path) -> None:
    '''Kills all processes with a handle in mount_path and unmounts it'''
    kill_procs_using_path(mount_path)
    all_mounted = get_mounted_info()
    all_mounted.reverse()
    for mounted in all_mounted:
        if Path(mounted.fs_file) == mount_path:
            unmount_path(mount_path)
