from subprocess import CompletedProcess


def exec_cmd(cmd: str, cmd_input: bytes = ...) -> CompletedProcess[bytes]: ...
