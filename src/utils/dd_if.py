

from pathlib import Path
from utils.subprocess_util import exec_cmd


def append_zeroes(image_path: Path, megabytes: int) -> Path:
    cmd = exec_cmd(
        f"dd if=/dev/zero bs=1M oflag=append conv=notrunc count={megabytes} of={image_path}")

    return Path(cmd.stdout.decode().strip())
