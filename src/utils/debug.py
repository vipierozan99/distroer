
from pprint import pprint
from dataclasses import asdict
from typing import Callable, Tuple

from colorama import init, Fore

from utils.losetup_if import find_unused_device

init(autoreset=True)


def wrap_in_messages(message: str, func: Callable):
    loading_msg(message)
    func()
    done_msg(message)


def loading_msg(message: str) -> None:
    print(f"[{Fore.BLUE}LOADING{Fore.RESET}] {message}",
          end="\r", flush=True)


def done_msg(message: str) -> None:
    print(f"[{Fore.GREEN}DONE{Fore.RESET}] {message}", flush=True)


def error_msg(message: str) -> None:
    print(f"[{Fore.RED}ERROR{Fore.RESET}] {message}", flush=True)


def pprint_dc(dc):
    pprint(asdict(dc))
