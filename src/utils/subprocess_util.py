import subprocess as sb
from subprocess import CompletedProcess
import shlex


def exec_cmd(cmd: str, cmd_input: bytes = None) -> CompletedProcess:
    process = sb.run(shlex.split(cmd), capture_output=True, input=cmd_input)
    if process.returncode != 0:
        raise sb.SubprocessError(process.stderr.decode())

    return process
