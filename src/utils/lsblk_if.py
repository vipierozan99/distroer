from __future__ import annotations
from typing import List, Dict
from dataclasses import dataclass, field
import json
from utils.subprocess_util import exec_cmd


@dataclass
class BlockDeviceInfo:
    name: str
    maj: int
    min: int
    rm: bool
    size: int  # in bytes
    ro: bool
    type: str
    mountpoint: str
    children: List[BlockDeviceInfo] = field(default_factory=list)

    @staticmethod
    def from_dict(dict: Dict) -> BlockDeviceInfo:
        maj, min = dict.get("maj:min").split(":")
        return BlockDeviceInfo(
            name=dict["name"],
            rm=dict["rm"],
            size=dict["size"],
            ro=dict["ro"],
            type=dict["type"],
            mountpoint=dict["mountpoint"],
            maj=int(maj),
            min=int(min),
            children=[BlockDeviceInfo.from_dict(
                child) for child in dict["children"]] if dict.get("children") else []
        )


def get_blkdevices_info() -> List[BlockDeviceInfo]:
    parsed_info: Dict = json.loads(exec_cmd(f"lsblk -pJbi").stdout)
    return [BlockDeviceInfo.from_dict(blk) for blk in parsed_info["blockdevices"]]
