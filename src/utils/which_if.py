

from pathlib import Path
import subprocess as sb
import shlex


# DEPRECATED: use shutil.which instead
def find_program_path(program: str, encoding="utf-8") -> Path:
    process = sb.run(shlex.split(f"which {program}"), capture_output=True)
    if process.returncode != 0:
        raise sb.SubprocessError(process.stderr.decode(encoding))

    return Path(process.stdout.decode(encoding).strip())
