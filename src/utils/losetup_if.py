

from pathlib import Path
from utils.subprocess_util import exec_cmd


def find_unused_device(encoding="utf-8") -> Path:
    cmd = exec_cmd("losetup -f")

    return Path(cmd.stdout.decode(encoding))
