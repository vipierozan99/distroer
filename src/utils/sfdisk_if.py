from __future__ import annotations
from pathlib import Path
from typing import Dict, List
from dataclasses import dataclass, field
import json
from .subprocess_util import exec_cmd


@dataclass
class PartitionInfo:
    node: str
    start: int
    size: int
    type: str

    @staticmethod
    def from_dict(dict: Dict) -> PartitionInfo:
        return PartitionInfo(
            node=dict["node"],
            start=dict["start"],
            size=dict["size"],
            type=dict["type"],
        )


@dataclass
class SfdiskInfo:
    label:  str
    id: str
    device: str
    unit: str
    partitions: List[PartitionInfo] = field(default_factory=list)

    @staticmethod
    def from_dict(dict: Dict) -> SfdiskInfo:
        info = SfdiskInfo(
            label=dict["label"],
            id=dict["id"],
            device=dict["device"],
            unit=dict["unit"],
        )

        for part in dict["partitions"]:
            info.partitions.append(PartitionInfo.from_dict(part))

        return info


def get_image_info(img_path: Path) -> SfdiskInfo:
    '''Calls sfdisk -dJ path and parses output'''
    cmd = exec_cmd(f"sfdisk -dJ {img_path}")

    return SfdiskInfo.from_dict(json.loads(cmd.stdout)["partitiontable"])


def apply_image_layout(img_path: Path, layout: SfdiskInfo) -> SfdiskInfo:
    '''Calls sfdisk and aplies a partitioning layout'''
    layout_text = ""
    for part in layout.partitions:
        layout_text += f"{part.node} : start= {part.start}, type={part.type} "
        if part.size:
            layout_text += f", size={part.size} "
        layout_text += "\n"

    exec_cmd(f'sfdisk {img_path}', cmd_input=bytes(layout_text, "utf-8"))

    return get_image_info(img_path)
