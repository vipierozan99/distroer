# wishbox_pi


## CUSTOM DISRTO TOOLS

- [yocto](https://www.yoctoproject.org/software-overview/)
- [CustomPiOs](https://github.com/guysoft/CustomPiOS)
- [Linux Live Kit](https://github.com/Tomas-M/linux-live)


## Virtualization

- https://doc.opensuse.org/documentation/leap/virtualization/html/book.virt/cha-libvirt-config-gui.html
- https://octetz.com/docs/2020/2020-05-06-linux-hypervisor-setup/
- https://www.youtube.com/watch?v=Y-FUvi1z1aU&t=193s
- https://ostechnix.com/access-and-modify-virtual-machine-disk-images-with-libguestfs/
- [This is it](https://github.com/dhruvvyas90/qemu-rpi-kernel)
- [seems better](https://blog.agchapman.com/using-qemu-to-emulate-a-raspberry-pi/)
- https://github.com/ivandavidov/minimal-linux-script
- https://github.com/ivandavidov/minimal
- https://github.com/Distrotech/xorriso
- https://buildroot.org/
