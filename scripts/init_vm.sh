#! /usr/bin/bash

sudo qemu-system-arm \
  -kernel ./kernels/kernel-qemu-4.19.50-buster \
  -dtb ./kernels/versatile-pb-buster.dtb \
  -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
  -hda $1 \
  -cpu arm1176 \
  -m 256 \
  -M versatilepb \
  -no-reboot \
  -serial stdio \
  -net nic -net user,hostfwd=tcp::5022-:22,hostfwd=tcp::5080-:80 \
  -net tap,ifname=vnet0,script=no,downscript=no
